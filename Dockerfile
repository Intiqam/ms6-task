FROM openjdk:16-alpine3.13
COPY build/libs/ms6-task-0.0.1-SNAPSHOT.jar /app/ms6-task.jar
WORKDIR /app/
ENTRYPOINT ["java","-jar","/app/ms6-task.jar"]