package com.ingress.ms6task.dto;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
@Data
public class VehicleRequest {
    private Long id;
    private String VIN;
    private String brand;
    private String model;
    private BigDecimal price;
}
