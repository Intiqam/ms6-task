package com.ingress.ms6task.exception;

public class VehicleNotFoundException extends RuntimeException{

    public VehicleNotFoundException(String s) {
        super(s);
    }
}
