package com.ingress.ms6task.exception;

public class ConstraintViolationExc extends RuntimeException {
    public ConstraintViolationExc(String s) {
        super(s);
    }
}
