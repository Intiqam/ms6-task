package com.ingress.ms6task.services;

import com.ingress.ms6task.dto.VehicleRequest;
import com.ingress.ms6task.dto.VehicleResponse;
import com.ingress.ms6task.exception.ConstraintViolationExc;
import com.ingress.ms6task.exception.VehicleNotFoundException;
import com.ingress.ms6task.model.Vehicle;
import com.ingress.ms6task.repository.VehicleRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class VehicleService {
    private final VehicleRepository vehicleRepository;
    private final ModelMapper modelMapper;

    public VehicleResponse create(VehicleRequest request) {
        boolean present = vehicleRepository.findByVIN(request.getVIN())
                .isPresent();
        if (present) throw new ConstraintViolationExc("VIN already exist = " + request.getVIN());
        Vehicle vehicle = modelMapper.map(request, Vehicle.class);
        return modelMapper.map(vehicleRepository.save(vehicle), VehicleResponse.class);
    }

    public VehicleResponse update(Long id, VehicleRequest request) {
        vehicleRepository.findById(id)
                .orElseThrow(() -> new VehicleNotFoundException("vehicle not found id = " + id));

        boolean present = vehicleRepository.findByVIN(request.getVIN())
                .isPresent();

        if (present) throw new ConstraintViolationExc("VIN already exist = " + request.getVIN());

        request.setId(id);

        return modelMapper.map(vehicleRepository.save(modelMapper.map(request, Vehicle.class)), VehicleResponse.class);
    }

    public void delete(Long id) {
        Vehicle vehicle = vehicleRepository.findById(id)
                .orElseThrow(() -> new VehicleNotFoundException("vehicle not found id = " + id));
        vehicleRepository.delete(vehicle);
    }

    public VehicleResponse get(Long id) {
        return vehicleRepository.findById(id)
                .map(v -> modelMapper.map(v, VehicleResponse.class))
                .orElseThrow(() -> new VehicleNotFoundException("vehicle not found id =" + id));

    }

    public List<VehicleResponse> getAll() {
        return vehicleRepository.findAll()
                .stream().map(v -> modelMapper.map(v, VehicleResponse.class))
                .collect(Collectors.toList());

    }

    public List<VehicleResponse> search(VehicleRequest searchFilter) {
        Example<Vehicle> example = Example.of(modelMapper.map(searchFilter, Vehicle.class));

        return vehicleRepository.findAll(example)
                .stream()
                .map(vehicle -> modelMapper.map(vehicle, VehicleResponse.class))
                .collect(Collectors.toList());
    }
}
