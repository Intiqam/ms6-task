package com.ingress.ms6task.repository;

import com.ingress.ms6task.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    Optional<Vehicle> findByVIN(String VIN);
}
