package com.ingress.ms6task.controller;

import com.ingress.ms6task.dto.VehicleRequest;
import com.ingress.ms6task.dto.VehicleResponse;
import com.ingress.ms6task.services.VehicleService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("v1/api/vehicles")
public class VehicleController {
    private final VehicleService vehicleService;

    @PostMapping
    public ResponseEntity<VehicleResponse> create(@RequestBody VehicleRequest request) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(vehicleService.create(request));
    }

    @PutMapping("/{id}")
    public ResponseEntity<VehicleResponse> update(@PathVariable Long id, @RequestBody VehicleRequest request) {
        return ResponseEntity
                .ok()
                .body(vehicleService.update(id, request));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        vehicleService.delete(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<VehicleResponse> get(@PathVariable Long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(vehicleService.get(id));
    }

    @GetMapping()
    public ResponseEntity<List<VehicleResponse>> getAll() {
        return ResponseEntity
                .ok()
                .body(vehicleService.getAll());

    }

    @PostMapping("/search")
    public ResponseEntity<List<VehicleResponse>> search(@RequestBody VehicleRequest searchTerm) {
        return ResponseEntity
                .ok()
                .body(vehicleService.search(searchTerm));

    }



}
